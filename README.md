Ce TP a pour but de mettre en place :
 - La stack ELK
 - Un NGINX + Filebeat


# Deploiement de la stack ELK via Docker-compose:
```bash
cd ELK && docker-compose up -d
```

# Deploiement d'un NGINX et Filebeat:
```bash
# Definition de l'IP d'ELK pour NGINX_Filebeat
IP_ELK="XXX.XXX.XXX.XXX"

# Se placer dans le bon repertoire :
cd NGINX_Filebeat/

# Creation d'un rep pour stocker les logs NGINX
[ ! -d nginx_logs ] && mkdir nginx_logs

# Lancer conteneur nginx 
docker run -d -p 8080:80 -v $PWD/nginx_logs:/var/log/nginx --name nginx nginx

# Lancer le conteneur fileabeat en initialisant et configure la connexion vers elastic et kibana
docker run \
docker.elastic.co/beats/filebeat:7.9.2 \
setup -E setup.kibana.host=${IP_ELK}:5601 \
-E output.elasticsearch.hosts=["${IP_ELK}:9200"]

# Effectue le remplacement de IP dans filebeat_docker.yml
sed "s/\${IP}/${IP_ELK}/g" template_filebeat_docker.yml > filebeat_docker.yml
# Donner les droits pour que user root puisse les lire dans le conteneur
chown root:root filebeat_docker.yml
chmod go-w filebeat_docker.yml

# Lancement du conteneur et configuration 
docker run -d \
  --name=filebeat \
  --user=root \
  --volume="$(pwd)/filebeat_docker.yml:/usr/share/filebeat/filebeat.yml" \
  --volume="$(pwd)/nginx.yml:/usr/share/filebeat/modules.d/nginx.yml" \
  --volume="/var/lib/docker/containers:/var/lib/docker/containers:ro" \
  --volume="/var/run/docker.sock:/var/run/docker.sock:ro" \
  --volume="$(pwd)/nginx_logs:/var/log/nginx:ro" \
  docker.elastic.co/beats/filebeat:7.9.2 filebeat -e --strict.perms=false


# Verification :
docker exec -ti filebeat /bin/bash
./filebeat setup --dashboards
ls -la /var/log/nginx/
ls -la /usr/share/filebeat/modules.d | grep nginx
./filebeat modules list | grep nginx
./filebeat modules enable nginx
./filebeat test config -e
./filebeat setup -e
```

